const path = require('path');

const camelCase = require('camelcase');
const slugify = require('@sindresorhus/slugify');
const pkg = require('./package.json');

const name = slugify(pkg.name);

module.exports = {
	mode: 'production',
	entry: path.resolve(__dirname, 'src'),
	output: {
		libraryTarget: 'umd',
		library: camelCase(name),
		filename: name + '.min.js',
		sourceMapFilename: name + '.map',
		path: path.resolve(__dirname, 'umd')
	},
	node: false,
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [['@babel/preset-env', {
							debug: true,
							useBuiltIns: 'usage'
						}]]
					}
				}
			}
		]
	}
};
