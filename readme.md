# mediatum-query-builder

> Convenience query builder for the RESTful [mediaTUM](https://github.com/mediatum/mediatum) API


## Install

```
$ npm install @ubw/mediatum-query-builder
```


## Usage

```js
const {raw, attributeMatch, and, or} = require('@ubw/mediatum-query-builder');

and(
    attributeMatch('author.surname', 'Dietrich'),
    attributeMatch('origin', 'Mathematik')
)
//=> '(author.surname="Dietrich") AND (origin="Mathematik")'
```


## Related

* [mediaTUM - Medienserver](https://mediatum.github.io)
* [mediaTUM Core](https://github.com/mediatum/mediatum)
* [@ubw/mediatum-rest-client](https://gitlab.com/ubw/mediatum-rest-client) - Convenience wrapper around the RESTful mediaTUM API


## License

MIT © [Michael Mayer](http://schnittstabil.de)
