import test from 'ava';
import {raw, attributeMatch, and, or} from '../src';

test('raw', t => {
	t.is(raw('unicorns'), 'unicorns');
	t.is(raw(), undefined);
	t.is(raw(''), undefined);
	t.is(raw(' '), undefined);
	t.is(raw(undefined), undefined);
	t.is(raw(null), undefined);
});

test('attributeMatch', t => {
	t.is(attributeMatch('unicorns', '42'), 'unicorns="42"');
	t.is(attributeMatch(undefined, '42'), '"42"');
	t.is(attributeMatch('unicorns', null), 'unicorns=""');
	t.is(attributeMatch('unicorns', undefined), undefined);

	t.is(attributeMatch('', ''), undefined);
	t.is(attributeMatch(' ', ' '), undefined);
	t.is(attributeMatch(undefined, undefined), undefined);
	t.is(attributeMatch(null, null), undefined);
	t.is(attributeMatch(), undefined);
});

test('and', t => {
	t.is(and(raw('unicorns')), 'unicorns');
	t.is(and(attributeMatch('unicorns', '42')), 'unicorns="42"');

	t.is(and(
		attributeMatch('unicorns', '42'),
		attributeMatch('leprechaun', '0'),
	), '(unicorns="42") AND (leprechaun="0")');

	t.is(and(
		raw('unicorns are awesome'),
		raw('the answer is 42')
	), '(unicorns are awesome) AND (the answer is 42)');

	t.is(and(), undefined);
	t.is(and('', ''), undefined);
	t.is(and(' ', ' '), undefined);
	t.is(and(undefined, undefined), undefined);
	t.is(and(null, null), undefined);
});

test('or', t => {
	t.is(or(raw('unicorns')), 'unicorns');
	t.is(or(attributeMatch('unicorns', '42')), 'unicorns="42"');

	t.is(or(
		attributeMatch('unicorns', '42'),
		attributeMatch('leprechaun', '0'),
	), '(unicorns="42") OR (leprechaun="0")');

	t.is(or(
		raw('unicorns are awesome'),
		raw('the answer is 42')
	), '(unicorns are awesome) OR (the answer is 42)');

	t.is(or(), undefined);
	t.is(or('', ''), undefined);
	t.is(or(' ', ' '), undefined);
	t.is(or(undefined, undefined), undefined);
	t.is(or(null, null), undefined);
});

test('usage', t => {
	t.is(and(
		attributeMatch('author.surname', 'Dietrich'),
		attributeMatch('origin', 'Mathematik')
	), '(author.surname="Dietrich") AND (origin="Mathematik")');
});
