const tokenToString = token => token === undefined ? undefined : token === null ? '' : String(token).trim();
const isEmpty = token => token === undefined || token === null || token === '';
const isNotEmpty = token => !isEmpty(token);

const raw = token => {
	token = tokenToString(token);
	return token === '' ? undefined : token;
};

const attributeMatch = (attribute, term) => {
	attribute = tokenToString(attribute);
	term = tokenToString(term);

	if (term === undefined) {
		return undefined;
	}

	if (isEmpty(attribute) && isEmpty(term)) {
		return undefined;
	}

	if (isEmpty(attribute)) {
		return `"${term}"`;
	}

	return `${attribute}="${term}"`;
};

const infixCompound = symbol => (...children) => {
	children = children.map(tokenToString).filter(isNotEmpty);

	if (children.length === 0) {
		return undefined;
	}

	if (children.length > 1) {
		children = children.map(child => `(${child})`);
	}

	return children.join(` ${symbol} `);
};

exports.raw = raw;
exports.attributeMatch = attributeMatch;
exports.and = infixCompound('AND');
exports.or = infixCompound('OR');
