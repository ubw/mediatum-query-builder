# Maintainer's Guide


## Prerequisites

```bash
yarn install
```


## Development

```bash
# lint, test and print covarage
yarn test

# watch ava-tests
yarn run test-watch
```


## Deployment

```bash
version=0.3.0

# ... edit version in package.json ...

yarn run build

git add . && git commit -m "$version" && git tag "$version"
git push && git push --tags

npm publish
```
